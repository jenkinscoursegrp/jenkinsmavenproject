package com.mavan.jenkinsProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsProjectApplication {

	public static void main(String[] args) {
		//static int x;
		//Main class
		SpringApplication.run(JenkinsProjectApplication.class, args);
		System.out.println("Hello I am main class");
	}

}
